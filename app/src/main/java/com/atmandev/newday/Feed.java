package com.atmandev.newday;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Feed {

    public static int MAXDESCRIPTIONSIZE = 100;

    public enum FeedType {
        TYPE_RSS, TYPE_HTML
    }
    public enum FeedSource {
        SRC_GOOGLETRENDS, SRC_WIKIPEDIA, SRC_NOWIKNOW, SRC_MENTALFLOSS
    }

    List<FeedItem> feedItems;
    InputStream rawData;
    private FeedType type;
    private FeedSource source;

    //Takes the raw string data of the xml/html, the type and source based on enums for this class
    public Feed(InputStream rawData, FeedType type, FeedSource source) {
        this.feedItems = new ArrayList<>();
        this.rawData = rawData;
        this.type = type;
        this.source = source;
    }

    public void addItem(FeedItem newItem) {
        if(source.equals(FeedSource.SRC_GOOGLETRENDS)) {
            newItem.setSourceName(Constants.GOOGLETRENDSNAME);
        } else if(source.equals(FeedSource.SRC_WIKIPEDIA)) {
            newItem.setSourceName(Constants.WIKIPEDIANAME);
        } else if(source.equals(FeedSource.SRC_MENTALFLOSS)) {
            newItem.setSourceName(Constants.MENTALFLOSSNAME);
            //newItem.setPictureUrl("http://mentalfloss.com/sites/default/files/logo_mental_floss.png");
        } else if(source.equals(FeedSource.SRC_NOWIKNOW)) {
            newItem.setSourceName(Constants.NOWIKNOWNAME);
            //newItem.setPictureUrl("http://nowiknow.com/wp-content/themes/now-i-know/img/logo.png");
        }

        String description = newItem.getDescription();
        if(description.length() >= MAXDESCRIPTIONSIZE) {
            newItem.setDescription(description.substring(0, MAXDESCRIPTIONSIZE) + "...");
        }

        feedItems.add(newItem);
    }

    public void addItems(List<FeedItem> newItems) {
        for(FeedItem f : newItems) {
            addItem(f);
        }
    }

    public List<FeedItem> getItems() {
        return feedItems;
    }

    public InputStream getRawData() {
        return rawData;
    }

    public void clearRawData() {rawData = null;}

    public FeedType getType() {
        return type;
    }

    public FeedSource getSource() {
        return source;
    }
}
