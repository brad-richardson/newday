package com.atmandev.newday;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Brad on 11/4/2014.
 */
public class FeedItem {
    private String name, description, url, pictureUrl, pubDateString, sourceName, newsHeadliner;

    private byte[] picture;

    public FeedItem (String name, String url) {
        this.name = name;
        this.url = url;
        description = pictureUrl = pubDateString = sourceName = newsHeadliner = "";
    }

    private Date formatDate(String newDate) {
        try {
            //Fri, 07 Nov 2014 09:00:00 -0800
            return new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
                    .parse(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) { this.sourceName = sourceName; }

    public String getNewsHeadliner() {
        return newsHeadliner;
    }

    public void setNewsHeadliner(String newsHeadliner) { this.newsHeadliner = newsHeadliner; }

    public String getPubDateString() {
        return sourceName;
    }

    public void setPubDateString(String pubDateString) {
        this.pubDateString = pubDateString;
    }

    public byte[] getPicture() {return picture; }

    public void setPicture(byte[] pic) { this.picture = pic; }

}
