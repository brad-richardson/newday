package com.atmandev.newday;

/**
 * Created by Brad on 12/5/2014.
 */
public class Constants {

    public static final String GOOGLETRENDSURL = "http://www.google.com/trends/hottrends/atom/feed?pn=p1";
    public static final String MENTALFLOSSURL = "http://mentalfloss.com/rss.xml";
    public static final String NOWIKNOWARCHIVE = "http://nowiknow.com/archives/";
    public static final String WIKIPEDIAURL = "http://en.wikipedia.org/wiki/Wikipedia:Today%27s_featured_article";
    public static final String GOOGLETRENDSNAME = "Google Trends";
    public static final String WIKIPEDIANAME = "Wikipedia";
    public static final String MENTALFLOSSNAME = "Mental Floss";
    public static final String NOWIKNOWNAME = "Now I Know";

    public static final int NOTIFICATIONID = 12345;
    public static final int BACKGROUNDSERVICEINTENTCODE = 56789;
    public static final String PREFSUPDATE = "LastUpdate";

    public static final int HOUR_TO_WAKE = 9;

}
