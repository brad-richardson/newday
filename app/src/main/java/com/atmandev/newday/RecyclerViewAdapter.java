package com.atmandev.newday;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private int mItemsInDb;
    Context mContext;
    SharedUtils mSharedUtils;

    public RecyclerViewAdapter (int itemsInDb, Context context) {
        mItemsInDb = itemsInDb;
        mContext = context;
        mSharedUtils = new SharedUtils();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        protected TextView mTitle;
        protected TextView mSubtitle;
        protected TextView mSource;
        protected ImageView mPicture;
        protected String url;
        protected int position;

        public ViewHolder(CardView v) {
            super(v);
            mPicture = (ImageView) v.findViewById(R.id.picture);
            mTitle = (TextView) v.findViewById(R.id.title);
            mSubtitle = (TextView) v.findViewById(R.id.subtitle);
            mSource = (TextView) v.findViewById(R.id.source);
            v.setOnClickListener(this);
        }

        public void onClick(View v) {
            // launch intent to show url from urlList
            String urlToLaunch = url;
            //Log.d(DEBUG_TAG ,"Uri to launch: " + urlToLaunch);
            if(!urlToLaunch.startsWith("http")) {
                urlToLaunch = "http://" + urlToLaunch;
            } else if (urlToLaunch.contains("trends")) {
                urlToLaunch = "https://www.google.com/?#q=" + mTitle.getText();
            }
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlToLaunch));

            mContext.startActivity(browserIntent);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder((CardView)v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(SharedUtils.mTitleCache.size() > position) {
            holder.mTitle.setText(SharedUtils.mTitleCache.get(position));
        }
        if(SharedUtils.mSubtitleCache.size() > position) {
            holder.mSubtitle.setText(SharedUtils.mSubtitleCache.get(position));
        }
        if(SharedUtils.mSourceCache.size() > position) {
            String sourceName = SharedUtils.mSourceCache.get(position);
            holder.mSource.setText("[" + sourceName + "]");
            switch (sourceName) {
                case Constants.WIKIPEDIANAME:
                    holder.mSource.setTextColor(Color.parseColor("#3F51B5"));
                    break;
                case Constants.GOOGLETRENDSNAME:
                    holder.mSource.setTextColor(Color.parseColor("#009688"));
                    break;
                case Constants.MENTALFLOSSNAME:
                    holder.mSource.setTextColor(Color.parseColor("#03A9F4"));
                    break;
                case Constants.NOWIKNOWNAME:
                    holder.mSource.setTextColor(Color.parseColor("#F44336"));
                    break;
            }
        }

        holder.position = position;
        holder.url = "http://www.google.com";
        new AsyncTask<ViewHolder, Void, FeedItem>() {
            private ViewHolder v;

            @Override
            protected FeedItem doInBackground(ViewHolder... params) {
                v = params[0];
                DbAdapter dbAdapter = new DbAdapter(mContext);
                dbAdapter.open();
                FeedItem feedItem = dbAdapter.getFeedItem(v.position);
                dbAdapter.close();
                return feedItem;
            }

            @Override
            protected void onPostExecute(FeedItem feedItem) {
                super.onPostExecute(feedItem);

                v.mTitle.setText(feedItem.getName());

                String sourceName = feedItem.getSourceName();
                v.mSource.setText("[" + sourceName + "]");
                switch (sourceName) {
                    case Constants.WIKIPEDIANAME:
                        v.mSource.setTextColor(Color.parseColor("#3F51B5"));
                        break;
                    case Constants.GOOGLETRENDSNAME:
                        v.mSource.setTextColor(Color.parseColor("#009688"));
                        break;
                    case Constants.MENTALFLOSSNAME:
                        v.mSource.setTextColor(Color.parseColor("#03A9F4"));
                        break;
                    case Constants.NOWIKNOWNAME:
                        v.mSource.setTextColor(Color.parseColor("#F44336"));
                        break;
                }

                //Get headliner or description
                if (!feedItem.getNewsHeadliner().equals("")) {
                    v.mSubtitle.setText(feedItem.getNewsHeadliner());
                } else if (!feedItem.getDescription().equals("")) {
                    v.mSubtitle.setText(feedItem.getDescription());
                } else {
                    v.mSubtitle.setText("");
                }

                v.url = feedItem.getUrl();

                // Get picture and show it
                byte[] pictureByteArray = feedItem.getPicture();
                if (pictureByteArray != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(pictureByteArray, 0, pictureByteArray.length);
                    v.mPicture.setVisibility(View.VISIBLE);
                    v.mPicture.setImageBitmap(bitmap);
                } else {
                    v.mPicture.setVisibility(View.GONE);
                }
            }
        }.execute(holder);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItemsInDb;
    }
}
