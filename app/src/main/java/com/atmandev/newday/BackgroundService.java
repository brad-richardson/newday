package com.atmandev.newday;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class BackgroundService extends Service implements AsyncResponse {

    SharedUtils sharedUtils;

    public BackgroundService() {
        sharedUtils = new SharedUtils();
        sharedUtils.delegate = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sharedUtils.downloadContent(this);

        sharedUtils.showNotification(this);

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void processFinish() {
        //do nothing
    }
}
