package com.atmandev.newday;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class SharedUtils implements AsyncResponse{
    public AsyncResponse delegate=null; // just keeps passing along

    public static ArrayList<String> mTitleCache = new ArrayList<>();
    public static ArrayList<String> mSubtitleCache = new ArrayList<>();
    public static ArrayList<String> mSourceCache = new ArrayList<>();
    public static int mItemsInDb = 0;

    // When user clicks button, calls AsyncTask.
    // Before attempting to fetch the URL, makes sure that there is a network connection.
    public void downloadContent(Context context) {
        // Reset all values
        mItemsInDb = 0;
        mTitleCache.clear();
        mSubtitleCache.clear();
        mSourceCache.clear();
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();
        dbAdapter.clear();
        dbAdapter.close();

        // Check for connection and start downloading
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            String[] toDownload = {Constants.WIKIPEDIAURL, Constants.GOOGLETRENDSURL, Constants.MENTALFLOSSURL, Constants.NOWIKNOWARCHIVE};

            for(String url : toDownload) {
                DownloadPageTask downloadPageTask = new DownloadPageTask(context);
                downloadPageTask.delegate = this;
                downloadPageTask.execute(url);
            }

            setLastUpdate(context);
        } else {
            Toast.makeText(context, "No network connection available.", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public String getLastUpdate(Context context) {
        // Gets previous update date
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREFSUPDATE, "");
    }

    public void setLastUpdate(Context context) {
        // Stores previous update date
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd");
        String dateNow = formatter.format(currentDate.getTime());

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.PREFSUPDATE, dateNow);
        editor.apply();
    }

    public void showNotification(Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.sun_icon)
                        .setContentTitle("New Day")
                        .setContentText("See top trends!")
                        .setCategory(NotificationCompat.CATEGORY_RECOMMENDATION)
                        .setPriority(NotificationCompat.PRIORITY_LOW)
                        .setAutoCancel(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent); // resultPendingIntent
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(Constants.NOTIFICATIONID, mBuilder.build()); // notificationid is some value to access the alarm later
    }

    public void showDelayedNotification(Context context) {
        Intent intent = new Intent(context, BackgroundService.class);
        PendingIntent pintent = PendingIntent.getService(context, Constants.BACKGROUNDSERVICEINTENTCODE, intent, 0);

        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pintent);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, Constants.HOUR_TO_WAKE);
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY,  pintent);
        //alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ 30000, pintent); for testing soon
    }

    @Override
    public void processFinish() {
        delegate.processFinish();
    }
}
