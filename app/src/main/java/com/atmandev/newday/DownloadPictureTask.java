package com.atmandev.newday;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadPictureTask extends AsyncTask<String,Void,String> {
    Context mContext;

    public DownloadPictureTask (Context context) {
        mContext = context;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            return downloadUrl(params[0], params[1]);
        } catch (IOException e) {
            return "Unable to connect to given url.";
        }
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    private String downloadUrl(String myurl, String id) throws IOException {
        InputStream is = null;

        try {
            if(!myurl.startsWith("http:")) {
                myurl = "http:" + myurl;
            }
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();

            is = conn.getInputStream();

            byte[] pictureByteArray = IOUtils.toByteArray(is);

            DbAdapter dbAdapter = new DbAdapter(mContext);
            dbAdapter.open();
            dbAdapter.updatePictureAtId(id, pictureByteArray);
            dbAdapter.close();

        } finally {
            if (is != null) {
                is.close();
            }
        }
        return "Done.";
    }

    @Override
    protected void onPostExecute(String result) {
        //showContent();
    }
}