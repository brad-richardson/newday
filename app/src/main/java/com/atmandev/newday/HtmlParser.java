package com.atmandev.newday;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by Brad on 11/7/2014.
 */
public class HtmlParser {

    private final int MAXITEMS = 5;

    public void parseFeed(Feed inputFeed) {
        String html = null;
        try {
            html = IOUtils.toString(inputFeed.getRawData(), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        inputFeed.clearRawData();
        Document doc = Jsoup.parse(html);
        html = null;

        if(inputFeed.getSource() == Feed.FeedSource.SRC_NOWIKNOW) {
            parseNowIKnow(inputFeed, doc);
        } else if (inputFeed.getSource() == Feed.FeedSource.SRC_WIKIPEDIA) {
            parseWikipedia(inputFeed, doc);
        }

    }

    private void parseNowIKnow(Feed inputFeed, Document doc) {
        Element archiveDiv = doc.select("div.archsp").first();
        Elements allATags = archiveDiv.select("a");

        int elementCount = 1;
        for (Element aTag : allATags) {
            if(elementCount > MAXITEMS) break;
            FeedItem newItem = new FeedItem(aTag.text(), aTag.attr("href"));
            inputFeed.addItem(newItem);
            elementCount++;
        }
    }

    private void parseWikipedia(Feed inputFeed, Document doc) {
        Elements featuredArticles = doc.select("td[style=color: #000]");

        for (Element article : featuredArticles) {
            String name, description, url, pictureUrl, date = "";

            Element firstATag = article.select("p").select("a").first();
            name = firstATag.attr("title");
            url = "en.wikipedia.org" + firstATag.attr("href");

            description = article.text();

            Element img = article.select("img").first();
            pictureUrl = img.attr("src");

            FeedItem newItem = new FeedItem(name, url);
            newItem.setDescription(description);
            newItem.setPictureUrl(pictureUrl);
            newItem.setPubDateString(date);

            inputFeed.addItem(newItem);
        }
    }
}
