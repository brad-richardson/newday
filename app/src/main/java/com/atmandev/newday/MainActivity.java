package com.atmandev.newday;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends Activity implements AsyncResponse {

    SharedUtils sharedUtils = new SharedUtils();
    RecyclerView.Adapter mAdapter;

    public static MainActivity mainActivity;

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        sharedUtils.delegate = this;

        if(wasYesterday(sharedUtils.getLastUpdate(mainActivity))) {
            sharedUtils.downloadContent(mainActivity);
        } else {
            // Just load data from sqlite
            DbAdapter dbAdapter = new DbAdapter(mainActivity);
            dbAdapter.open();
            SharedUtils.mItemsInDb = (int)dbAdapter.getItemCount();
            dbAdapter.close();
        }

        setAdapter();

        forceShowMenuButton();
    }

    public void forceShowMenuButton() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        }
        catch (Exception e) {
            // presumably, not relevant
        }
    }

    public boolean wasYesterday(String date) {
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd");
        String dateNow = formatter.format(currentDate.getTime());

        return !date.equals(dateNow);
    }
    public void setAdapter() {
        // Refreshes the views
        mAdapter = new RecyclerViewAdapter(SharedUtils.mItemsInDb, mainActivity);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            sharedUtils.downloadContent(mainActivity);
        } else if (id == R.id.action_notification) {
            sharedUtils.showNotification(mainActivity);
        } else if (id == R.id.action_delay_notification) {
            sharedUtils.showDelayedNotification(mainActivity);
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void processFinish() {
        setAdapter();
    }

}
