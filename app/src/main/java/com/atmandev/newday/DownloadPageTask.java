package com.atmandev.newday;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadPageTask extends AsyncTask<String,Void,String> {
    public AsyncResponse delegate=null;

    FeedParser feedParser;
    HtmlParser htmlParser;
    Context mContext;

    public DownloadPageTask (Context context) {
        mContext = context;
        feedParser = new FeedParser();
        htmlParser = new HtmlParser();
    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            for(String url : urls) {
                downloadPage(url);
            }
            return "Finished downloading pages";
        } catch (IOException e) {
            return "Unable to connect to given url.";
        }
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    private String downloadPage(String myurl) throws IOException {
        InputStream is = null;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            //int response = conn.getResponseCode();
            //Log.d(DEBUG_TAG, "The response is: " + response);
            is = conn.getInputStream();

            Feed newFeed = null;
            switch (myurl) {
                case Constants.GOOGLETRENDSURL:
                    newFeed = new Feed(is, Feed.FeedType.TYPE_RSS, Feed.FeedSource.SRC_GOOGLETRENDS);
                    break;
                case Constants.MENTALFLOSSURL:
                    newFeed = new Feed(is, Feed.FeedType.TYPE_RSS, Feed.FeedSource.SRC_MENTALFLOSS);
                    break;
                case Constants.NOWIKNOWARCHIVE:
                    newFeed = new Feed(is, Feed.FeedType.TYPE_HTML, Feed.FeedSource.SRC_NOWIKNOW);
                    break;
                case Constants.WIKIPEDIAURL:
                    newFeed = new Feed(is, Feed.FeedType.TYPE_HTML, Feed.FeedSource.SRC_WIKIPEDIA);
                    break;
            }

            if(newFeed.getType() == Feed.FeedType.TYPE_RSS) {
                feedParser.parseFeed(newFeed);
            } else if (newFeed.getType() == Feed.FeedType.TYPE_HTML) {
                htmlParser.parseFeed(newFeed);
            }

            DbAdapter dbAdapter = new DbAdapter(mContext);
            dbAdapter.open();
            for(FeedItem fi : newFeed.getItems()) {
                // Add items to cache to make it look fast
                SharedUtils.mTitleCache.add(fi.getName());
                if(!fi.getNewsHeadliner().equals("")) {
                    SharedUtils.mSubtitleCache.add(fi.getNewsHeadliner());
                } else if (!fi.getDescription().equals("")) {
                    SharedUtils.mSubtitleCache.add(fi.getDescription());
                } else {
                    SharedUtils.mSubtitleCache.add("");
                }
                SharedUtils.mSourceCache.add(fi.getSourceName());

                final long insertLocation = dbAdapter.insertFeedItem(fi);

                if(myurl.equals(Constants.MENTALFLOSSURL)) {
                    Drawable drawable = mContext.getResources().getDrawable(R.drawable.mental_floss);
                    Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    dbAdapter.updatePictureAtId(String.valueOf(insertLocation), stream.toByteArray());
                } else if (myurl.equals(Constants.NOWIKNOWARCHIVE)) {
                    Drawable drawable = mContext.getResources().getDrawable(R.drawable.nowiknow_logo);
                    Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    dbAdapter.updatePictureAtId(String.valueOf(insertLocation), stream.toByteArray());
                } else if(!fi.getPictureUrl().equals("")) {
                    DownloadPictureTask pictureTask = new DownloadPictureTask(mContext);
                    pictureTask.execute(fi.getPictureUrl(), String.valueOf(insertLocation));
                }
                SharedUtils.mItemsInDb++;
            }
            dbAdapter.close();

        } finally {
            if (is != null) {
                is.close();
            }
        }
        return "Done.";
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish();
    }
}
