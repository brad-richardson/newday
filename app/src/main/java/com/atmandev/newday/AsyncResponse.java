package com.atmandev.newday;

/**
 * Created by Brad on 12/5/2014.
 */
public interface AsyncResponse {
    void processFinish();
}
