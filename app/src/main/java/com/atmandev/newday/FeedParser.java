package com.atmandev.newday;

import android.text.Html;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brad on 11/4/2014.
 */
public class FeedParser {

    public void parseFeed(Feed inputFeed) {
        XmlParser xmlParser = new XmlParser();

        try {
            List<FeedItem> parsedItems = xmlParser.parse(inputFeed.getRawData());
            inputFeed.addItems(parsedItems);
            inputFeed.clearRawData();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class XmlParser {
        // We don't use namespaces
        private final String ns = null;

        public List parse(InputStream in) throws XmlPullParserException, IOException {
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in, null);
                parser.nextTag();
                return readFeed(parser);
            } finally {
                in.close();
            }
        }

        private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
            List items = new ArrayList();

            parser.require(XmlPullParser.START_TAG, ns, "rss");
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();
                // Starts by looking for the entry tag
                if (name.equals("item")) {
                    items.add(readItem(parser));
                } else if (name.equals("channel")) {
                     // do nothing, just enter the field
                } else {
                    skip(parser);
                }
            }
            return items;
        }


        // Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
        // to their respective "read" methods for processing. Otherwise, skips the tag.
        private FeedItem readItem(XmlPullParser parser) throws XmlPullParserException, IOException {
            parser.require(XmlPullParser.START_TAG, ns, "item");
            String title = "";
            String description = "";
            String headliner = "";
            String link = "";
            String pictureUrl = "";
            String pubDate = ""; // no longer being used
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();
                if (name.equals("title")) {
                    title = readTitle(parser);
                } else if (name.equals("link")) {
                    link = readLink(parser);
                } else if (name.equals("description")) {
                    description = readDescription(parser);
                } else if (name.equals("ht:picture")) {
                    pictureUrl = readPictureUrl(parser);
                } else if (name.equals("pubDate")) {
                    pubDate = readPubDate(parser);
                } else if (name.equals("ht:news_item")) {
                    headliner = readHeadliner(parser);
                } else {
                    skip(parser);
                }
            }
            FeedItem newItem = new FeedItem(title, link);
            String noHTMLDescription = description.replaceAll("\\<.*?>","");
            newItem.setDescription(noHTMLDescription);
            String noHTMLHeadliner = Html.fromHtml(headliner).toString();
            newItem.setNewsHeadliner(noHTMLHeadliner);
            newItem.setPictureUrl(pictureUrl);
            newItem.setPubDateString(pubDate);
            return newItem;
        }

        // Processes title tags in the feed.
        private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "title");
            String title = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "title");
            return title;
        }

        private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "description");
            String description = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "description");
            return description;
        }

        // Processes link tags in the feed.
        private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "link");
            String link = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "link");
            return link;
        }

        // Processes summary tags in the feed.
        private String readPictureUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "ht:picture");
            String pictureUrl = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "ht:picture");
            return pictureUrl;
        }

        private String readPubDate(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "pubDate");
            String pubDate = readText(parser);
            parser.require(XmlPullParser.END_TAG, ns, "pubDate");
            return pubDate;
        }

        private String readHeadliner(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, ns, "ht:news_item");
            String headliner = "";
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();
                if(name.equals("ht:news_item_title") && headliner.equals("")) {
                    parser.require(XmlPullParser.START_TAG, ns, "ht:news_item_title");
                    headliner = readText(parser);
                    parser.require(XmlPullParser.END_TAG, ns, "ht:news_item_title");
                } else {
                    skip(parser);
                }
            }
            //parser.require(XmlPullParser.END_TAG, ns, "ht:news_item");
            return headliner;
        }

        // For the tags title and summary, extracts their text values.
        private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
            String result = "";
            if (parser.next() == XmlPullParser.TEXT) {
                result = parser.getText();
                parser.nextTag();
            }
            return result;
        }

        private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                throw new IllegalStateException();
            }
            int depth = 1;
            while (depth != 0) {
                switch (parser.next()) {
                    case XmlPullParser.END_TAG:
                        depth--;
                        break;
                    case XmlPullParser.START_TAG:
                        depth++;
                        break;
                }
            }
        }

    }
}
