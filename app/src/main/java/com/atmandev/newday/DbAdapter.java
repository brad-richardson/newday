package com.atmandev.newday;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Brad on 12/3/2014.
 */
public class DbAdapter {

    public static final String LOGTAG = DbAdapter.class.getSimpleName() + "_TAG";

    public static final String DB_NAME = "newday.db";
    public static final int DB_VERSION = 1;
    public static final String NEWDAY_TABLE = "newday";

    public static final String NEWDAY_ID_COL_NAME = "ID";
    public static final String NEWDAY_NAME_COL_NAME = "Name";
    public static final String NEWDAY_URL_COL_NAME = "URL";
    public static final String NEWDAY_PICTURE_URL_COL_NAME = "PictureURL";
    public static final String NEWDAY_DESCRIPTION_COL_NAME = "Description";
    public static final String NEWDAY_NEWS_HEADLINER_COL_NAME = "NewsHeadliner";
    public static final String NEWDAY_SOURCE_NAME_COL_NAME = "SourceName";
    public static final String NEWDAY_PUB_DATE_COL_NAME = "PubDate";
    public static final String NEWDAY_PICTURE_COL_NAME = "Picture";

    private SQLiteDatabase mDb = null;
    private Context mContext = null;
    private NewDayDbOpenHelper mDbHelper = null;

    public DbAdapter(Context context) {
        mContext = context;
        mDbHelper = new NewDayDbOpenHelper(context, DB_NAME, null, DB_VERSION);
    }

    public void clear() {
        mDb.execSQL("DROP TABLE IF EXISTS " + NEWDAY_TABLE);
        mDbHelper.onCreate(mDb);
    }

    public long insertFeedItem(FeedItem fi) {
        ContentValues newFeedItem = new ContentValues();
        newFeedItem.put(NEWDAY_NAME_COL_NAME, fi.getName());
        newFeedItem.put(NEWDAY_URL_COL_NAME, fi.getUrl());
        newFeedItem.put(NEWDAY_PICTURE_URL_COL_NAME, fi.getPictureUrl());
        newFeedItem.put(NEWDAY_DESCRIPTION_COL_NAME, fi.getDescription());
        newFeedItem.put(NEWDAY_NEWS_HEADLINER_COL_NAME, fi.getNewsHeadliner());
        newFeedItem.put(NEWDAY_SOURCE_NAME_COL_NAME, fi.getSourceName());
        newFeedItem.put(NEWDAY_PUB_DATE_COL_NAME, fi.getPubDateString());

        long insertedRowIndex = mDb.insert(NEWDAY_TABLE, null, newFeedItem);
        //Log.d("DBTAG", "Inserted record: " + insertedRowIndex + " \"" + fi.getName() + "\"");
        return insertedRowIndex;
    }

    public FeedItem getFeedItem(int id) {

        // Do query to find item at given id
        Cursor c = mDb.rawQuery("SELECT * FROM " + NEWDAY_TABLE + " WHERE "
                + NEWDAY_ID_COL_NAME + " = " + (id + 1), null);

        if(c.moveToFirst()) {
            // Setup new feed item from data in db
            FeedItem feedItemFromDb = new FeedItem(c.getString(c.getColumnIndex(NEWDAY_NAME_COL_NAME)),
                    c.getString(c.getColumnIndex(NEWDAY_URL_COL_NAME)));
            feedItemFromDb.setPictureUrl(c.getString(c.getColumnIndex(NEWDAY_PICTURE_URL_COL_NAME)));
            feedItemFromDb.setDescription(c.getString(c.getColumnIndex(NEWDAY_DESCRIPTION_COL_NAME)));
            feedItemFromDb.setNewsHeadliner(c.getString(c.getColumnIndex(NEWDAY_NEWS_HEADLINER_COL_NAME)));
            feedItemFromDb.setSourceName(c.getString(c.getColumnIndex(NEWDAY_SOURCE_NAME_COL_NAME)));
            feedItemFromDb.setPubDateString(c.getString(c.getColumnIndex(NEWDAY_PUB_DATE_COL_NAME)));
            feedItemFromDb.setPicture(c.getBlob(c.getColumnIndex(NEWDAY_PICTURE_COL_NAME)));
            return feedItemFromDb;
        }
        if(c != null && !c.isClosed()) {
            c.close();
        }

        return new FeedItem("", "http://www.google.com");
    }

    public void updatePictureAtId(String id, byte[] picture) {
        String strFilter = NEWDAY_ID_COL_NAME + " = " + id;
        ContentValues args = new ContentValues();
        args.put(NEWDAY_PICTURE_COL_NAME, picture);
        mDb.update(NEWDAY_TABLE, args, strFilter, null);
    }

    public long getItemCount() {
        Cursor c = mDb.rawQuery("SELECT * FROM " + NEWDAY_TABLE + " WHERE 1 = 1", null);
        if(c.moveToFirst()) {
            return c.getCount();
        }
        return 0;
    }

    // open either writeable or, if that is impossible,
    // readable database
    public void open() throws SQLiteException {
        try {
            mDb = mDbHelper.getWritableDatabase();
            //Log.d(LOGTAG, "WRITEABLE DB CREATED");
        }
        catch ( SQLiteException ex ) {
            //Log.d(LOGTAG, "READABLE DB CREATED");
            mDb = mDbHelper.getReadableDatabase();
        }
    }

    public void close() {
        mDb.close();
    }

    private static class NewDayDbOpenHelper extends SQLiteOpenHelper {

        static final String NEWDAY_TABLE_CREATE =
                "create table " + NEWDAY_TABLE +
                        " (" +
                        NEWDAY_ID_COL_NAME            + " integer primary key autoincrement, " +
                        NEWDAY_NAME_COL_NAME  + " text not null, " +
                        NEWDAY_URL_COL_NAME     + " text not null, " +
                        NEWDAY_PICTURE_URL_COL_NAME     + " text, " +
                        NEWDAY_DESCRIPTION_COL_NAME     + " text, " +
                        NEWDAY_NEWS_HEADLINER_COL_NAME     + " text, " +
                        NEWDAY_SOURCE_NAME_COL_NAME     + " text not null, " +
                        NEWDAY_PUB_DATE_COL_NAME     + " text not null, " +
                        NEWDAY_PICTURE_COL_NAME         + " blob" +
                        ");";

        public NewDayDbOpenHelper(Context context, String name,
                                         SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(NEWDAY_TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + NEWDAY_TABLE);
            onCreate(db);
        }
    }

}
